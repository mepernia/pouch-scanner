<?php

namespace PouchScanner\Domain\Exceptions;

use Exception;

class LoginPouchScannerException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
